# SFDX CI

## Environments

- [x] Main org and devhub peter.dejong@youngblood.nu.dev
- [x] DevEd org for namespace peter.dejong@youngblood.nu.deved

## Create Connected app

### Create Certificate

- [x] Installeer OpenSSL 64bit
- [x] `mkdir certificates`
- [x] `openssl genrsa -des3 -passout pass:<password> -out server.pass.key 2048`
- [x] `openssl rsa -passin pass:<password> -in server.pass.key -out server.key`
- [x] `rm server.pass.key`
- [x] `openssl req -new -key server.key -out server.csr`
- [x] `openssl x509 -req -sha256 -days 365 -in server.csr -signkey server.key -out server.crt`

### Create App

- [x] From Setup, enter App Manager in the Quick Find box, then select App Manager.
- [x] Click New Connected App.
- [x] Enter the connected app name and your email address:
- [x] Connected App Name: `sfdx ci`
- [x] Contact Email: <your email address>
- [x] Select Enable OAuth Settings.
- [x] Enter the callback URL: 
- [x] `http://localhost:1717/OauthRedirect`
- [x] Select Use digital signatures.
- [x] To upload your server.crt file, click Choose File.
- [x] For OAuth scopes, add:
- [x] Access and manage your data (api)
- [x] Perform requests on your behalf at any time (refresh_token, offline_access)
- [x] Provide access to your data via the Web (web)
- [x] Click Save

### Edit policies to avoid authorization step

- [ ] Click Manage
- [ ] Click Edit Policies
- [ ] Enable Admin approved users are pre-authorized
- [ ] Save

### Create permission set

- [x] From Setup, enter Permission in the Quick Find box, then select Permission Sets.
- [x] Click New.
- [x] For the Label, enter: sfdx ci
- [x] Click Save.
- [x] Click sfdx ci | Manage Assignments | Add Assignments.
- [x] Select the checkbox next to your Dev Hub username, then click Assign | Done.
- [x] Go back to your connected app.
- [x] From Setup, enter App Manager in the Quick Find box, then select App Manager.
- [x] Next to sfdx ci, click the list item drop-down arrow (), then select Manage.
- [x] In the Permission Sets section, click Manage Permission Sets.
- [x] Select the checkbox next to sfdx ci, then click Save.

## Test JWT Auth Flow

Log in to DevHub: 
`sfdx force:auth:web:login -d -a DevHub`

Log in using JWT Auth Flow:
`sfdx force:auth:jwt:grant -i <CONSUMERKEY> -u <username> -f server.key`

## Create Git repository

Create repository online. Example name `sfdxci`.

Clone the repository:
`git clone <url/sfdxci>`

## Initialize Project

From the parent of the `sfdxci` directory, run:
`sfdx force:project:create -n sfdxci`.

Add the developer hub org:
`sfdx force:auth:web:login -d -a DevHub`

## Create Docker Image

### Install and run Docker Desktop

Log in to docker hub.

### Create Image

Create sfdxci directory and place Dockerfile:
```
FROM node:jessie
RUN apt-get update
RUN apt-get -y install git openssh-client ca-certificates openssl curl
RUN npm install sfdx-cli --global
RUN sfdx --version
USER node
```

Within directory run:
`docker build -t <username>/sfdxci .`

Test the image:
`docker run <username>/sfdxci sfdx —version`

Push to Docker Hub:
`docker push <username>/sfdxci`

## Create BitBucket Pipeline

Under Pipelines define `bitbucket-pipelines.yml`:
```
image: nathema/sfdxci:latest
pipelines:
  default:
  - step:
      script:
      - echo $SFDC_PROD_URL
      - echo $SFDC_PROD_USER
      - sfdx force:auth:jwt:grant --clientid $CONSUMER_KEY --username $SFDC_PROD_USER --jwtkeyfile keys/server.key --setdefaultdevhubusername --setalias sfdx-ci --instanceurl $SFDC_PROD_URL
      - sfdx force:source:convert -d mdapi
      - sfdx force:mdapi:deploy -d mdapi -u $SFDC_PROD_USER
```

In the settings define the variables:
- `SFDC_PROD_URL`
- `SFDC_PROD_USER`
- `CONSUMER_KEY`

If we would run now we would find an error at the convert stage saying `The package root directory is empty`.

## Create a change with a scratch org

### Generate scratch org

Do:
`sfdx force:org:create -f config\project-scratch-def.json —setalias scratch`

Open the newly create scratch org with:
`sfdx force:org:open -u scratch`

### Do a change

Create a random field on Contact, like a picklist with someone’s Favourite Mascotte.

After do:
`sfdx force:source:pull -u scratch`

And commit the changes.

Our changes are automatically deployed to the org using our pipeline. 
